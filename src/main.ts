/* eslint-disable @typescript-eslint/semi */
import path from 'path'
import * as vscode from 'vscode'

export function activate(context: vscode.ExtensionContext) {
	context.subscriptions.push(vscode.commands.registerCommand('pick-file-command.quick-pick', async (arg) => {
		if (!vscode.workspace.workspaceFolders?.length) {
			return
		}

		const isTask =
				arg instanceof Array &&
				arg.length === 2 &&
				typeof arg[0] === 'string' &&
				typeof arg[1] === 'string' &&
				arg[0].startsWith('${command:')
		const isDebug =
				typeof arg === 'object' &&
				arg['pick-file-command.quick-pick'] &&
				arg.type &&
				arg.mode &&
				arg.request

		let dir: vscode.Uri | undefined
		if (isDebug) {
			dir = vscode.Uri.parse(arg['pick-file-command.quick-pick'])

		} else if (isTask) {
			const m = (arg[0] as string).match(/\$\{command:pick-file-command.quick-pick(?::(?<args>.+)?)?\}/)
			dir = vscode.Uri.parse(arg[1] as string)
			if (m?.groups?.args) {
				if (path.isAbsolute(m.groups.args)) {
					dir = vscode.Uri.parse(m.groups.args)
				} else {
					dir = vscode.Uri.joinPath(dir, m.groups.args)
				}
			}
		}

		if (!dir) {
			if (vscode.workspace.workspaceFolders.length === 1) {
				dir = vscode.workspace.workspaceFolders[0].uri
			} else {
				dir = await chooseWorkspace()
				if (!dir) { return }
			}
		}

		while (true) {
			// @ts-ignore
			const { type, uri } = await chooseFile(dir!)
			if (!uri) { return }

			if (type !== vscode.FileType.Directory) {
				return uri.fsPath
			} else {
				dir = uri
			}
		}
	}))
}

export function deactivate() {}

async function chooseWorkspace(): Promise<vscode.Uri | undefined> {
	const ws = vscode.workspace.workspaceFolders!
	const items = ws.map(({ name, uri }) => ({ label: name, uri }))
	const r = await vscode.window.showQuickPick(items, { title: 'Select Workspace' })
	if (!r) {
		return
	}

	return r.uri
}

async function chooseFile(dir: vscode.Uri): Promise<{ uri?: vscode.Uri, type: vscode.FileType }> {
	const { name, type } = await vscode.window.showQuickPick((async () => {
		const entries = await vscode.workspace.fs.readDirectory(dir)
		const items = entries.map(([name, type]) => {
			const label = type === vscode.FileType.Directory ? `${name}/` : name
			return { label, name, type }
		})
		items.unshift({ label: '../', name: '..', type: vscode.FileType.Directory })
		return items
	})(), { title: 'Select File' }) || { type: vscode.FileType.Unknown }
	if (!name) {
		return { type }
	}

	const uri = vscode.Uri.joinPath(dir, name)
	return { uri, type }
}