# Pick File Command

Provides a command for quick-picking files, intended for use with launch
configurations or tasks.

By default, `pick-file-command.quick-pick` will ask you to select a workspace,
then ask you to select a file in that workspace.

## `launch.json`

If you specify `"pick-file-command.quick-pick": "<dir>"` in a launch
configuration, `quick-pick` will start in that directory - **however this may
not work for all debug adapters**.

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            // ...
            "args": ["${command:pick-file-command.quick-pick}"],
            "pick-file-command.quick-pick": "${workspaceFolder}/foo/bar"
        },
    ]
}
```

## `tasks.json`

By default, `quick-pick` will start in the task's workspace. If you specify a
relative path after the command, `quick-pick` will interpret the path relative
to the task's workspace and start there. If you specify an absolute path after
the command, `quick-pick` will start there.

```json
{
    "version": "2.0.0",
    "tasks": [
        {
            // ...
            "args": [
                "${command:pick-file-command.quick-pick:foo/bar}",
            ],
        }
    ]
}
```